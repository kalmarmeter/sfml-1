#include <SFML/Graphics.hpp>
#include <filesystem>
#include <iostream>

sf::BlendMode blendMode(sf::BlendMode::DstColor, sf::BlendMode::Zero, sf::BlendMode::Add, sf::BlendMode::DstAlpha, sf::BlendMode::Zero, sf::BlendMode::Add);

sf::Sprite maskSprite(sf::Sprite& sprite, sf::Sprite& mask, const int spriteTextureSize) {

	static sf::RenderTexture renderTexture;

	renderTexture.create(spriteTextureSize, spriteTextureSize);
	renderTexture.clear(sf::Color::Transparent);
	sprite.setPosition(0, 0);
	mask.setPosition(0, 0);
	renderTexture.draw(sprite);
	renderTexture.draw(mask, blendMode);

	renderTexture.display();

	sf::Sprite result(renderTexture.getTexture());

	return result;

}

std::experimental::filesystem::path resourceDirectory = std::experimental::filesystem::current_path() / ".." / "Resources";

const int inputFloorTileCount = 16;
const int inputBlockTileCount = 8;

const int inputTileCount = inputFloorTileCount + inputBlockTileCount;

const int overlayCount = 16;
const int triangleCount = 4;
const int tileTextureSize = 8;

int main() {
	
	sf::Texture textureFloorOverlays;
	textureFloorOverlays.loadFromFile((resourceDirectory / "overlay_floor.png").string());

	sf::Texture textureBlockOverlays;
	textureBlockOverlays.loadFromFile((resourceDirectory / "overlay_block.png").string());

	sf::Texture textureFloorTiles;
	textureFloorTiles.loadFromFile((resourceDirectory / "tiles_floor.png").string());

	sf::Texture textureBlockTiles;
	textureBlockTiles.loadFromFile((resourceDirectory / "tiles_block.png").string());

	sf::Texture textureTriangles;
	textureTriangles.loadFromFile((resourceDirectory / "overlay_triangles.png").string());

	sf::Sprite inputFloorOverlay;
	inputFloorOverlay.setTexture(textureFloorOverlays);

	sf::Sprite inputBlockOverlay;
	inputBlockOverlay.setTexture(textureBlockOverlays);

	sf::Sprite inputFloorTile;
	inputFloorTile.setTexture(textureFloorTiles);

	sf::Sprite inputBlockTile;
	inputBlockTile.setTexture(textureBlockTiles);

	sf::Sprite inputTriangle;
	inputTriangle.setTexture(textureTriangles);

	sf::RenderTexture renderTexture;
	renderTexture.create(inputTileCount * tileTextureSize, (overlayCount + triangleCount) * tileTextureSize);

	std::cout << "--------------" << std::endl;

	//Side blends
	for (int tile = 0; tile < inputTileCount; ++tile) {

		sf::Sprite inputTile;
		sf::Sprite inputOverlay;
		int tileIndex;
		if (tile < inputFloorTileCount) {
			inputTile = inputFloorTile;
			inputOverlay = inputFloorOverlay;
			tileIndex = tile;
		}
		else {
			inputTile = inputBlockTile;
			inputOverlay = inputBlockOverlay;
			tileIndex = tile - inputFloorTileCount;
		}

		for (int overlay = 0; overlay < overlayCount; ++overlay) {

			inputTile.setTextureRect(sf::IntRect(tileIndex * tileTextureSize, 0, tileTextureSize, tileTextureSize));
			inputOverlay.setTextureRect(sf::IntRect(overlay * tileTextureSize, 0, tileTextureSize, tileTextureSize));

			sf::Sprite result = maskSprite(inputTile, inputOverlay, tileTextureSize);

			result.setPosition(tile * tileTextureSize, overlay * tileTextureSize);

			renderTexture.draw(result);

		}

		std::cout << "Generated overlay blend for tile #" << tile << std::endl;
	}

	std::cout << "--------------" << std::endl;

	//Triangle blends
	for (int tile = 0; tile < inputTileCount; ++tile) {

		sf::Sprite inputTile;
		int tileIndex;
		if (tile < inputFloorTileCount) {
			inputTile = inputFloorTile;
			tileIndex = tile;
		}
		else {
			inputTile = inputBlockTile;
			tileIndex = tile - inputFloorTileCount;
		}

		for (int triangle = 0; triangle < triangleCount; ++triangle) {

			inputTile.setTextureRect(sf::IntRect(tileIndex * tileTextureSize, 0, tileTextureSize, tileTextureSize));
			inputTriangle.setTextureRect(sf::IntRect(triangle * tileTextureSize, 0, tileTextureSize, tileTextureSize));

			sf::Sprite result = maskSprite(inputTile, inputTriangle, tileTextureSize);

			result.setPosition(tile * tileTextureSize, overlayCount * tileTextureSize + triangle * tileTextureSize);

			renderTexture.draw(result);

		}

		std::cout << "Generated triangle blend for tile #" << tile << std::endl;
	}

	std::cout << "--------------" << std::endl;

	renderTexture.display();

	sf::Image output(renderTexture.getTexture().copyToImage());

	output.saveToFile((resourceDirectory / "tiles_overlaid.png").string());

	std::cout << "File saved to: " << (resourceDirectory / "tiles_overlaid.png").string() << std::endl;
	std::cin.get();


}
