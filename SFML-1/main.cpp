#include <SFML/Graphics.hpp>
#include <functional>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <sstream>

const int spriteTextureSize = 8;
const int spriteInGameSize = 48;
const float spriteScaleFactor = (float)spriteInGameSize / spriteTextureSize;
const int blockSpriteOffset = 16;

int random_range(int lower, int higher) {

	if (lower == higher)
		return lower;

	int diff = (higher - lower);
	return rand() % diff + lower;
}

struct TileSpriteData {

	std::vector<int> textureIndices;
	int spriteVariants;
	int spriteLevel;
	float scale;

	TileSpriteData(const std::vector<int>& textureIndices = {}, int spriteLevel = 0, float scale = 1.0f) {
		this->spriteVariants = textureIndices.size();
		this->textureIndices = textureIndices;
		this->spriteLevel = spriteLevel;
		this->scale = scale;
	}
};

enum class TileType {
	Floor, Block
};

struct TileData {

	static int count;

	int id;
	std::string name;
	TileSpriteData spriteData;
	TileType type;

	TileData() {}

	TileData(const std::string& name, const TileSpriteData& spriteData, TileType type) {
		this->id = count++;
		this->name = name;
		this->spriteData = spriteData;
		this->type = type;
	}

};

int TileData::count = 0;

using TileAtlas = std::map<std::string, TileData>;
using TileIDAtlas = std::vector<TileData>;

TileAtlas tileAtlas;
TileIDAtlas tileIDAtlas;

void loadTileData(const std::string& filename) {

	std::ifstream input(filename);

	if (input.fail()) {
		std::cout << "Failed to open file at: " << filename << std::endl;
		return;
	}

	std::string currentLine;
	while (!input.eof()) {

		std::getline(input, currentLine);
		std::cout << currentLine << std::endl;

		if (currentLine[0] == '#' || currentLine[0] == '/n' || currentLine.empty())
			continue;

		std::string idName = currentLine;

		std::getline(input, currentLine);
		std::string name = currentLine;

		std::getline(input, currentLine);
		TileType type;
		if (currentLine == "FLOOR")
			type = TileType::Floor;
		else if (currentLine == "BLOCK")
			type = TileType::Block;
		else {
			std::cout << "Invalid tile type " << currentLine << std::endl;
			return;
		}

		std::getline(input, currentLine);
		std::vector<int> textureIndices;
		std::stringstream strStream(currentLine);
		int index;
		while (strStream >> index) {
			textureIndices.push_back(index);
		}

		int level;
		input >> level;

		tileAtlas[idName] = TileData(name, TileSpriteData(textureIndices, level), type);
		tileIDAtlas.push_back(TileData(name, TileSpriteData(textureIndices, level), type));

		std::cout << "Loaded tile << " << idName << " >>" << std::endl;

	}

	input.close();

}

class Coord {
private:
	int _x, _y;
public:
	Coord(int x = 0, int y = 0) : _x(x), _y(y) {}
	Coord operator+(const Coord& rhs) const {
		return Coord(_x + rhs._x, _y + rhs._y);
	}
	bool operator==(const Coord& rhs) const {
		return _x == rhs._x && _y == rhs._y;
	}
	bool operator!=(const Coord& rhs) const {
		return !(*this == rhs);
	}
	int x() const {
		return _x;
	}
	int y() const {
		return _y;
	}
	static Coord invalid;
};

Coord Coord::invalid = Coord(999'999, 999'999);



class Tile {
private:
	int spriteVariant;
	const TileData* data;

public:
	Tile(const TileData& data) {
		this->data = &data;
		this->spriteVariant = random_range(0, this->data->spriteData.spriteVariants);
	}
	int id() const {
		return data->id;
	}
	std::string name() const {
		return data->name;
	}
	TileType type() const {
		return data->type;
	}
	int textureIndex() const {
		if (type() == TileType::Floor)
			return data->spriteData.textureIndices.at(spriteVariant);
		else
			return data->spriteData.textureIndices.at(spriteVariant) + blockSpriteOffset;
	}
	int spriteLevel() const {
		return data->spriteData.spriteLevel;
	}
};


class Map {
private:	
	int _width, _height;
	std::vector<Tile> _tiles;
	int coordToIndex(const Coord& coord) const {
		return coord.x() + coord.y() * _width;
	}

public:
	Map(int width = 0, int height = 0) : _width(width), _height(height) {
		_tiles.resize(width * height, Tile(tileAtlas.at("FLOOR_DIRT")));
	}
	bool isValidCoord(const Coord& pos) const {
		return (pos.x() >= 0 && pos.y() >= 0 && pos.x() < _width && pos.y() < _height);
	}
	void setTile(const Coord& at, const Tile& newTile) {
		if (isValidCoord(at)) {
			_tiles[coordToIndex(at)] = newTile;
		}
	}
	Tile getTile(const Coord& at) const {
		if (isValidCoord(at)) { return _tiles[coordToIndex(at)];
		}
	}
	int width() const {
		return _width;
	}
	int height() const {
		return _height;
	}
};

const std::experimental::filesystem::path resourceDirectory = std::experimental::filesystem::current_path() / ".." / "Resources";


int main() {

	const int mapWidth = 20;
	const int mapHeight = 20;

	const int windowWidth = mapWidth * spriteInGameSize;
	const int windowHeight = mapHeight * spriteInGameSize;

	loadTileData((resourceDirectory / "tiles_data.txt").string());

	sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "Things", sf::Style::Close | sf::Style::Titlebar);

	Map map(mapWidth, mapHeight);

	sf::Texture textureTiles;
	textureTiles.loadFromFile((resourceDirectory / "tiles_overlaid.png").string());

	bool leftPressed = false;
	bool rightPressed = false;

	int currentTileID = 1;

	float daytime = 0.0f;


	while (window.isOpen()) {

		window.clear();

		sf::Event event;
		while (window.pollEvent(event)) {

			if (event.type == sf::Event::Closed) {
				window.close();
			}

			if (event.type == sf::Event::MouseButtonPressed) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					leftPressed = true;
				}
				else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					rightPressed = true;
				}
			}

			if (event.type == sf::Event::MouseButtonReleased) {
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					leftPressed = false;
				}
				if (event.mouseButton.button == sf::Mouse::Button::Right) {
					rightPressed = false;
				}
			}

			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Q) {
					currentTileID = std::max(0, currentTileID - 1);
					std::cout << "Selected tile: " << tileIDAtlas.at(currentTileID).name << std::endl;
				}
				if (event.key.code == sf::Keyboard::E) {
					currentTileID = std::min(TileData::count - 1, currentTileID + 1);
					std::cout << "Selected tile: " << tileIDAtlas.at(currentTileID).name << std::endl;
				}
				if (event.key.code == sf::Keyboard::N) {
					daytime = std::max(-1.0f, daytime - 0.05f);
				}
				if (event.key.code == sf::Keyboard::M) {
					daytime = std::min( 1.0f, daytime + 0.05f);
				}
			}


		}

		if (leftPressed) {
			Coord mouseTile(sf::Mouse::getPosition(window).x / spriteInGameSize, sf::Mouse::getPosition(window).y / spriteInGameSize);
			map.setTile(mouseTile, Tile(tileIDAtlas[currentTileID]));
		} else if (rightPressed) {
			Coord mouseTile(sf::Mouse::getPosition(window).x / spriteInGameSize, sf::Mouse::getPosition(window).y / spriteInGameSize);
			map.setTile(mouseTile, Tile(tileIDAtlas[currentTileID]));
		}

		std::vector<Coord> blockCoords;

		for (int x = 0; x < mapWidth; ++x) {
			for (int y = 0; y < mapHeight; ++y) {

				Tile currentTile = map.getTile(Coord(x, y));
				
				sf::Sprite sprite;
				sprite.setPosition(x * spriteInGameSize, y * spriteInGameSize);
				sprite.setTexture(textureTiles);
				sprite.setScale(spriteScaleFactor, spriteScaleFactor);

				bool block = currentTile.type() == TileType::Block;
				bool northBlock = map.isValidCoord(Coord(x, y - 1)) && map.getTile(Coord(x, y - 1)).type() == TileType::Block;
				bool southBlock = map.isValidCoord(Coord(x, y + 1)) && map.getTile(Coord(x, y + 1)).type() == TileType::Block;
				bool eastBlock  = map.isValidCoord(Coord(x + 1, y)) && map.getTile(Coord(x + 1, y)).type() == TileType::Block;
				bool westBlock  = map.isValidCoord(Coord(x - 1, y)) && map.getTile(Coord(x - 1, y)).type() == TileType::Block;

				bool floor = currentTile.type() == TileType::Floor;
				bool northFloor = map.isValidCoord(Coord(x, y - 1)) && map.getTile(Coord(x, y - 1)).type() == TileType::Floor;
				bool southFloor = map.isValidCoord(Coord(x, y + 1)) && map.getTile(Coord(x, y + 1)).type() == TileType::Floor;
				bool eastFloor  = map.isValidCoord(Coord(x + 1, y)) && map.getTile(Coord(x + 1, y)).type() == TileType::Floor;
				bool westFloor  = map.isValidCoord(Coord(x - 1, y)) && map.getTile(Coord(x - 1, y)).type() == TileType::Floor;

				bool northHigher = map.isValidCoord(Coord(x, y - 1)) && map.getTile(Coord(x, y - 1)).spriteLevel() > currentTile.spriteLevel();
				bool southHigher = map.isValidCoord(Coord(x, y + 1)) && map.getTile(Coord(x, y + 1)).spriteLevel() > currentTile.spriteLevel();
				bool eastHigher  = map.isValidCoord(Coord(x + 1, y)) && map.getTile(Coord(x + 1, y)).spriteLevel() > currentTile.spriteLevel();
				bool westHigher  = map.isValidCoord(Coord(x - 1, y)) && map.getTile(Coord(x - 1, y)).spriteLevel() > currentTile.spriteLevel();

				int northFloorBorder = (floor && northFloor && northHigher) || (block && northFloor);
				int southFloorBorder = (floor && southFloor && southHigher) || (block && southFloor);
				int eastFloorBorder  = (floor && eastFloor && eastHigher) || (block && eastFloor);
				int westFloorBorder = (floor && westFloor && westHigher) || (block && westFloor);

				int floorIndex = eastFloorBorder | (northFloorBorder << 1) | (westFloorBorder << 2) | (southFloorBorder << 3);

				Tile northTile = map.getTile(Coord(x, y - 1));
				Tile southTile = map.getTile(Coord(x, y + 1));
				Tile eastTile = map.getTile(Coord(x + 1, y));
				Tile westTile = map.getTile(Coord(x - 1, y));

				if (northFloorBorder) {
					sprite.setTextureRect(sf::IntRect(northTile.textureIndex() * spriteTextureSize, (16 + 1) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

					window.draw(sprite);
				}
				if (southFloorBorder) {
					sprite.setTextureRect(sf::IntRect(southTile.textureIndex() * spriteTextureSize, (16 + 3) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

					window.draw(sprite);
				}
				if (eastFloorBorder) {
					sprite.setTextureRect(sf::IntRect(eastTile.textureIndex() * spriteTextureSize, (16 + 2) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

					window.draw(sprite);
				}
				if (westFloorBorder) {
					sprite.setTextureRect(sf::IntRect(westTile.textureIndex() * spriteTextureSize, (16 + 0) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

					window.draw(sprite);
				}
				if (floor) {
					sprite.setTextureRect(sf::IntRect(currentTile.textureIndex() * spriteTextureSize, floorIndex * spriteTextureSize, spriteTextureSize, spriteTextureSize));

					window.draw(sprite);
				}
				if (block) {
					blockCoords.push_back(Coord(x, y));
				}

			}
		}

		//Shadow drawing
		//...

		for (Coord& coord : blockCoords) {


			int x = coord.x();
			int y = coord.y();

			sf::Sprite sprite;
			sprite.setTexture(textureTiles);
			sprite.setScale(sf::Vector2f(spriteScaleFactor, spriteScaleFactor));
			sprite.setPosition(x * spriteInGameSize, y * spriteInGameSize);


			Tile currentTile = map.getTile(coord);

			bool northBlock = map.isValidCoord(Coord(x, y - 1)) && map.getTile(Coord(x, y - 1)).type() == TileType::Block;
			bool southBlock = map.isValidCoord(Coord(x, y + 1)) && map.getTile(Coord(x, y + 1)).type() == TileType::Block;
			bool eastBlock  = map.isValidCoord(Coord(x + 1, y)) && map.getTile(Coord(x + 1, y)).type() == TileType::Block;
			bool westBlock  = map.isValidCoord(Coord(x - 1, y)) && map.getTile(Coord(x - 1, y)).type() == TileType::Block;

			Tile northTile = map.getTile(Coord(x, y - 1));
			Tile southTile = map.getTile(Coord(x, y + 1));
			Tile eastTile = map.getTile(Coord(x + 1, y));
			Tile westTile = map.getTile(Coord(x - 1, y));

			bool northHigher = northBlock && northTile.spriteLevel() > currentTile.spriteLevel();
			bool southHigher = southBlock && southTile.spriteLevel() > currentTile.spriteLevel();
			bool eastHigher  = eastBlock  && eastTile.spriteLevel()  > currentTile.spriteLevel();
			bool westHigher  = westBlock  && westTile.spriteLevel()  > currentTile.spriteLevel();

			bool northFloor = map.isValidCoord(Coord(x, y - 1)) && northTile.type() == TileType::Floor;
			bool southFloor = map.isValidCoord(Coord(x, y + 1)) && southTile.type() == TileType::Floor;
			bool eastFloor  = map.isValidCoord(Coord(x + 1, y)) && eastTile.type()  == TileType::Floor;
			bool westFloor  = map.isValidCoord(Coord(x - 1, y)) && westTile.type()  == TileType::Floor;

			int northBlockBorder = northHigher || northFloor;
			int southBlockBorder = southHigher || southFloor;
			int eastBlockBorder  = eastHigher  || eastFloor;
			int westBlockBorder  = westHigher  || westFloor;

			int blockIndex = eastBlockBorder | (northBlockBorder << 1) | (westBlockBorder << 2) | (southBlockBorder << 3);

			sprite.setTextureRect(sf::IntRect(currentTile.textureIndex() * spriteTextureSize, blockIndex * spriteTextureSize, spriteTextureSize, spriteTextureSize));
			
			sf::Sprite shadowSprite = sprite;
			shadowSprite.setColor(sf::Color(0, 0, 0, 100));

			if (daytime < 0.0f) {

				if (westFloor) {

					shadowSprite.setPosition(sprite.getPosition() + sf::Vector2f(daytime * 6.0f * spriteScaleFactor, 0));
					window.draw(shadowSprite);
				}

			}
			else {

				if (eastFloor) {

					shadowSprite.setPosition(sprite.getPosition() + sf::Vector2f(daytime * 6.0f * spriteScaleFactor, 0));
					window.draw(shadowSprite);

				}

			}

			if (northHigher) {
				sprite.setTextureRect(sf::IntRect(northTile.textureIndex() * spriteTextureSize, (16 + 1) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

				window.draw(sprite);
			}
			if (southHigher) {
				sprite.setTextureRect(sf::IntRect(southTile.textureIndex() * spriteTextureSize, (16 + 3) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

				window.draw(sprite);
			}
			if (eastHigher) {
				sprite.setTextureRect(sf::IntRect(eastTile.textureIndex() * spriteTextureSize, (16 + 2) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

				window.draw(sprite);
			}
			if (westHigher) {
				sprite.setTextureRect(sf::IntRect(westTile.textureIndex() * spriteTextureSize, (16 + 0) * spriteTextureSize, spriteTextureSize, spriteTextureSize));

				window.draw(sprite);
			}


			sprite.setTextureRect(sf::IntRect(currentTile.textureIndex() * spriteTextureSize, blockIndex * spriteTextureSize, spriteTextureSize, spriteTextureSize));

			

			window.draw(sprite);

		}

		sf::RectangleShape tint;
		tint.setSize(sf::Vector2f(window.getSize().x, window.getSize().y));

		if (daytime < -0.4f) {
			tint.setFillColor(sf::Color(153, 204, 255, 255 * 0.3f * (std::abs(daytime) - 0.4f)));
			window.draw(tint);
		}
		else if (daytime > 0.6f) {
			tint.setFillColor(sf::Color(255, 51, 0, 255 * 0.3f * (daytime - 0.6f)));
			window.draw(tint);
		}

		window.display();

	}

}